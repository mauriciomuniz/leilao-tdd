package br.com.leilao;

import java.util.Objects;

public class Lance {

    private Usuario usuario;
    private double valorLance;

    public Lance() {
    }

    public Lance(Usuario usuario, double valorLance) {
        this.usuario = usuario;
        this.valorLance = valorLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValorLance() {
        return valorLance;
    }

    public void setValorLance(double valorLance) {
        this.valorLance = valorLance;
    }

    @Override
    public String toString() {
        return usuario +
                ", valorLance=" + valorLance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lance lance = (Lance) o;
        return Double.compare(lance.valorLance, valorLance) == 0 &&
                Objects.equals(usuario, lance.usuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usuario, valorLance);
    }
}
