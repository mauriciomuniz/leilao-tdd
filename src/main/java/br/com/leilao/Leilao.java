package br.com.leilao;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Leilao {

    private List<Lance> lances = new ArrayList<>();

    public Lance adicionarLance(Lance lance) {
        lances.add(validarLance(lance));
        return lances.get(indiceUltimoLance());
    }

    private Lance validarLance(Lance lance) {
        if (!lances.isEmpty()) {
            Lance ultimoLance = lances.get(indiceUltimoLance());
            if (lance.getValorLance() > ultimoLance.getValorLance()) {
                return lance;
            } else {
                throw new RuntimeException("O Valor do ultimo lance é: " + ultimoLance + " - Favor adicionar um lance maior que o ultimo");
            }
        }
        return lance;
    }

    private int indiceUltimoLance() {
        int quantidadeDeLances = lances.size();
        return (quantidadeDeLances - 1);
    }

    @Override
    public String toString() {
        return "Leilao{" +
                "lances=" + lances +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Leilao leilao = (Leilao) o;
        return Objects.equals(lances, leilao.lances);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lances);
    }
}
