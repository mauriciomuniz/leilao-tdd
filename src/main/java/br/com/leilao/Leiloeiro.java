package br.com.leilao;

public class Leiloeiro {

    private String nomeLeilao;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nomeLeilao, Leilao leilao) {
        this.nomeLeilao = nomeLeilao;
        this.leilao = leilao;
    }

    public String getNomeLeilao() {
        return nomeLeilao;
    }

    public void setNomeLeilao(String nomeLeilao) {
        this.nomeLeilao = nomeLeilao;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }
}
