package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class LeilaoTest {

    private Usuario usuario;
    private Lance lance;
    private Leilao leilao;

    @BeforeEach
    private void setUp(){
        this.usuario = new Usuario("Mauricio", 1);
        this.lance = new Lance(this.usuario, 20.0);
        this.leilao = new Leilao();
    }

    @Test
    public void testarAdicionarLance () {

        Lance lanceAdicionado = leilao.adicionarLance(this.lance);

        Assertions.assertEquals(20.0, lanceAdicionado.getValorLance());
    }

    @Test
    public void testarRetornarMaiorLance () {

        Lance lanceAdicionado = leilao.adicionarLance(this.lance);

        Usuario usuario2 = new Usuario("Maria", 2);
        Lance lance2 = new Lance(usuario2, 25.0);
        lanceAdicionado = leilao.adicionarLance(lance2);

        Usuario usuario3 = new Usuario("JOaquina", 3);
        Lance lance3 = new Lance(usuario3, 35.0);
        lanceAdicionado = leilao.adicionarLance(lance3);

        Assertions.assertEquals(35.0, lanceAdicionado.getValorLance());
        Assertions.assertEquals("JOaquina", lanceAdicionado.getUsuario().getNome());
    }
}
